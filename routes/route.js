var router = require('express').Router();
var path = require("path");

// Routes :
router.get("/admin", function(req, res) {
	res.sendFile(path.join(__dirname, '../public', 'html/admin.html'));
});

router.get('/', function(req, res) {
	res.sendFile(path.join(__dirname, '../public', 'html/home.html'));
});

router.get("/FAQ", function(req, res) {
	res.sendFile(path.join(__dirname, '../public', 'html/FAQ.html'));
});

router.get('/', function(req, res) {
  res.sendFile( path.join(__dirname, "../public", "html/home.html") );
});

module.exports = router;
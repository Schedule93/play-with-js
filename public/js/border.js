function main () {
	const warrning = [
		'background: white',
		'color: orange',
		'border-radius: 20px',
		'font-size: 12px',
		'border: 1px solid orange',
		'padding: 1px 2px 1px 10px;'
	].join(';');

	console.log('%cWarrning ', warrning, 'Write some text');

	const loading = [
		'background: white',
		'color: blue',
		'border-radius: 20px',
		'font-size: 12px',
		'border: 1px solid blue',
		'padding: 1px 0px 0px 0px;'
	].join(';');

	console.log('%c Loading ', loading, 'Write some text');

	const error = [
		'background: white',
		'color: red',
		'border-radius: 20px',
		'font-size: 12px',
		'border: 1px solid red',
		'padding: 1px 0px 0px 0px;'
	].join(';');

	console.log('%c Error ', error, 'Write some text');

	const successful = [
		'background: white',
		'color: green',
		'border-radius: 20px',
		'font-size: 12px',
		'border: 1px solid green',
		'padding: 1px 0px 0px 0px;'
	].join(';');

	console.log('%c Successful ', successful, 'Write some text.');

}

main();
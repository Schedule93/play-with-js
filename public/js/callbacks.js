
// Create a simple callback
function main(callback) {
	return callback();
}

function callback () {
	return "return callback";
}

main(function() {
	console.log(callback());
});
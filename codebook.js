let express = require('express');
let app = express();
var path = require('path');
const PORT = 10;
const router = require("./routes/route.js");
app.use("/", router);

// middleware
app.use( express.static( path.join(__dirname, 'public') ) );

// Server Listening :
app.listen(PORT, function() {
  console.log("App listening on port " + PORT );
});
